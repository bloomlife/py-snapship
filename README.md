# py-snapship

Provides a Python wrapper over Snapship's API.

## Usage

First create an instance of the `Snapship` object to initiate an API link:

    >>> import snapship
    >>> s = Snapship(secret_key='<Your Snapship secret key>')

Get documentation on any object or method using `help()`:

    >>> help(snapship.Destination)

List all the products:

    >>> s.products()
    [Product(sku='PRODUCT-01', name='My first product', inventory=Inventory(available=9999, inbound=1000, on_hand=10000, processing=1, reserved=0)), Product(sku='PRODUCT-02', name='My second product', inventory=Inventory(available=9993, inbound=0, on_hand=10000, processing=7, reserved=0))]

Place a new order with 6 items:

    >>> dest = snapship.Destination('La Batte', None, 'Liège', '4000', 'Liège', 'BE')
    >>> prod1 = snapship.OrderCreateProduct('PRODUCT-01', 1)
    >>> prod2 = snapship.OrderCreateProduct('PRODUCT-02', quantity=5)
    >>> s.order_create('ORDER-01', 'Raphael Javaux', '+32412345678', 'raphael@bloomlife.com', dest, products=[prod1, prod2])
    Order(id='EIYN-272A-XY', [..])

Get back order information with `order()` and `orders()`:

    >>> s.order('EIYN-272A-XY')
    Order(id='EIYN-272A-XY', [..])
    >>> s.orders()
    [Order(id='EIYN-272A-XY', [..]), [..]]

Create a new return for the order with only the first product:

    >>> ret_prod1 = snapship.ReturnCreateProduct('PRODUCT-01', reason='Requested by customer', note=None)
    >>> s.return_create('EIYN-272A-XY', [ret_prod1])

## Webhooks

The library provides two decorators to help you implementing Snapship webhooks
in a Flask application:

    from flask import Flask
    from snapship import webhooks

    app = Flask(__name__)

    @app.route('/webhook/snapship/orders', methods=['POST'])
    @webhooks.orders
    def handler(request_order):

        # Process any order change that occured on `request_order`

        return ('', 204)

    @app.route('/webhook/snapship/returns', methods=['POST'])
    @webhooks.returns
    def handler(request_ret):

        # Process any return change that occured on `request_ret`

        return ('', 204)

## Runs the tests

Don't use Pytest, but the TOX utility to run the unit tests:

    tox tests
