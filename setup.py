#!/usr/bin/env python3
# Bloom Technologies Inc. Copyright 2018
#
# Authors: Raphael Javaux <raphael@bloom-life.com>

from setuptools import setup

setup(
    name='py-snapship',
    version='0.1',

    description='Provides a Python wrapper over Snapship\'s API.',
    author='Raphael Javaux',
    author_email='raphael@bloomlife.com',
    url='https://bitbucket.org/bloomlife/py-snapship',
    license='GNU General Public License v3',

    packages=['snapship'],
    package_dir={'snapship': 'src/snapship'},

    install_requires=['Flask', 'requests',],
    setup_requires=['pytest-runner',],
    tests_require=['pytest', 'requests_mock',],

)
