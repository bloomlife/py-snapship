# Bloom Technologies Inc. Copyright 2018
#
# Authors: Raphael Javaux <raphael@bloom-life.com>

import pytest

from flask import Flask
from snapship import webhooks

from .test_snapship import TestSnapship

class TestWebhooks:
    def test_orders(self):
        order = TestSnapship._TestSnapship__gen_test_order_result()

        # Tests the webhook decorator on a temporary Flask app

        app = Flask(__name__)

        @app.route('/orders', methods=['POST'])
        @webhooks.orders
        def handler(request_order):
            assert request_order.id == order['id']
            return ('', 204)

        client = app.test_client()
        resp = client.post('/orders', json={'payload': order})

        assert resp.status_code == 204

    def test_returns(self):
        ret = TestSnapship._TestSnapship__gen_test_return_result()

        # Tests the webhook decorator on a temporary Flask app

        app = Flask(__name__)

        @app.route('/returns', methods=['POST'])
        @webhooks.returns
        def handler(request_ret):
            assert request_ret.id == ret['id']
            return ('', 204)

        client = app.test_client()
        resp = client.post('/returns', json={'payload': ret})

        assert resp.status_code == 204
