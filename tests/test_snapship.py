# Bloom Technologies Inc. Copyright 2018
#
# Authors: Raphael Javaux <raphael@bloom-life.com>

import math

import pytest, requests, requests_mock

import snapship

TEST_HOST = 'api.test.snapship.it'

class TestSnapship:
    def test_orders(self):
        with requests_mock.Mocker() as m:
            # Setups the mock response

            response = {
                'meta': {
                    'status': '200 OK',
                    'request_time': 0.6614439487457275,
                    'start_time': 1541438448.427207,
                    'end_time': 1541438449.088651,
                    'path': '/api/public/orders',
                    'method': 'GET',
                    'revision': '8474b1'
                },
                'total_pages': 1,
                'result': [TestSnapship.__gen_test_order_result()]
            }

            path = '/api/public/orders'
            m.get('https://' + TEST_HOST + '/api/public/orders', json=response)

            # Test the response

            s = snapship.Snapship(secret_key='', host=TEST_HOST)
            ret = s.orders()

            shipped_at = response['result'][0]['shipped_at']
            assert len(ret) == 1
            assert ret[0].shipped_at.timestamp() == \
                pytest.approx(shipped_at, 0.001)

    def test_products(self):
        with requests_mock.Mocker() as m:
            # Creates a product list mock endpoint with several pages

            limit = snapship.MAX_ITEMS_PER_PAGE
            total_pages = 3
            n_products = limit * total_pages

            def gen_product(sku):
                return {
                    'sku': str(sku),
                    'name': 'The Lion, the Witch, and the Wardrobe',
                    'inventory': {
                        'available': 0,
                        'inbound': 10,
                        'on_hand': 114,
                        'processing': 114,
                        'reserved': 1,
                    }
                }

            products = [gen_product(i) for i in range(0, n_products)]

            def response(page):
                page_products = products[(page - 1) * limit:page * limit]
                total_pages = math.ceil(len(products) / limit)

                return {
                    'meta': {
                        'status': '200 OK',
                        'request_time': 0.35317277908325195,
                        'start_time': 1540425348.685969,
                        'end_time': 1540425349.039142,
                        'path': '/api/public/products',
                        'method': 'GET',
                        'revision': '2b9296',
                    },
                    'total_pages': total_pages,
                    'result': page_products,
                }

            for page in range(1, total_pages + 1):
                path = '/api/public/products?limit={}&page={}'.format(
                    limit, page
                )
                m.get('https://' + TEST_HOST + path, json=response(page))

            # Test the response

            s = snapship.Snapship(secret_key='', host=TEST_HOST)
            ret = s.products()

            assert len(ret) == n_products
            assert ret[0].sku == '0'
            assert ret[-1].sku == str(n_products - 1)

    @staticmethod
    def __gen_test_order_result():
        return {
            'status': 'picking',
            'delivered_at': None,
            'reference': 'foobar',
            'tracking_url': 'https://track.snapship.io/',
            'recipient_name': 'Josef Lange',
            'recipient_email': 'josef@snapship.it',
            'speed': 'standard',
            'shipped_at': 1541438448.427207,
            'destination': {
                'state_province': 'WA',
                'city': 'Coupeville',
                'country': 'US',
                'postal_code': '98239',
                'first_line': '1229 Nimitz Dr',
                'second_line': None
            },
            'id': 'ACM5-7FOU-6I',
            'returns': [],
            'products': [
                {'sku': 'JRR04', 'serial': None},
                {'sku': 'BOOKMARK', 'serial': None}
            ],
            'recipient_telephone': '(262) 880-5412',
        }

    @staticmethod
    def __gen_test_return_result():
        return {
            'id': 'EMRI-SBKL-6N',
            'status': 'PENDING',
            'processed_at': 1541438448.427207,
            'shipped_at': 1541438448.427207,
            'delivered_at': None,
            'products': [{
                'sku': 'CSL-01',
                'received': None,
                'reason_note': 'Subscription Ended',
                'reason': 'OTHER',
                'reception_note': None,
                'serial': None,
            }],
            'label': 'https://snapship-uploads-alpha.s3.amazonaws.com/',
        }
