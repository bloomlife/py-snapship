# Bloom Technologies Inc. Copyright 2018
#
# Authors: Raphael Javaux <raphael@bloom-life.com>

from functools import wraps

from flask import request

from .snapship import Snapship

def orders(func):
    """
    Use this decorator on a Flask view to handle Snapship order events.
    """

    @wraps(func)
    def func_wrapper():
        result = request.json['payload']
        order = Snapship._Snapship__parse_order(result)
        return func(order)

    return func_wrapper

def returns(func):
    """
    Use this decorator on a Flask view to handle Snapship return events.
    """

    @wraps(func)
    def func_wrapper():
        result = request.json['payload']
        ret = Snapship._Snapship__parse_return(result)
        return func(ret)

    return func_wrapper
