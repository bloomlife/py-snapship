# Bloom Technologies Inc. Copyright 2018
#
# Authors: Raphael Javaux <raphael@bloom-life.com>

import datetime

import requests

from .exceptions import *
from .types import *

MAX_ITEMS_PER_PAGE = 25

class Snapship:
    def __init__(
        self, secret_key, protocol='https', host='api.snapship.it',
    ):
        self.secret_key = secret_key
        self.protocol = protocol
        self.host = host

    def order(self, sku):
        o = self.__make_request('/orders/{}'.format(sku))
        return Snapship.__parse_order(o)

    def orders(self):
        """
        Get the list of orders.

        Returns
        -------
        a list of `Order`s.
        """
        return [
            Snapship.__parse_order(o)
            for o in self.__make_request('/orders')
        ]

    def order_create(
        self, reference,
        recipient_name, recipient_telephone, recipient_email, destination,
        products
    ):
        """
        Creates a new order.

        Parameters
        ----------
        reference, recipient_name, recipient_telephone, recipient_email : str
        destination : Destination
        products : list of OrderCreateProduct

        Returns
        -------
        The newly created `Order` object.
        """

        payload = {
            'reference': reference,
            'recipient_name': recipient_name,
            'recipient_email': recipient_email,
            'recipient_telephone': recipient_telephone,
            'destination': {
                'first_line': destination.first_line,
                'second_line': destination.second_line,
                'city': destination.city,
                'postal_code': destination.postal_code,
                'state_province': destination.state_province,
                'country': destination.country,
            },
            'products': [
                {'sku': p.sku, 'quantity': p.quantity} for p in products
            ]
        }

        o = self.__make_request(
            '/orders', method=requests.post, payload=payload
        )
        return Snapship.__parse_order(o)

    def products(self):
        """
        Get the list of products and inventory.

        Returns
        -------
        a list of `Product`s.
        """
        return [
            Snapship.__parse_product(p)
            for p in self.__make_request('/products')
        ]

    def returns(self):
        """
        Get the list of product returns.

        Returns
        -------
        a list of `Return`s.
        """
        return [
            Snapship.__parse_return(r)
            for r in self.__make_request('/returns')
        ]

    def return_create(self, order_id, products):
        """
        Creates a new return for an order.

        Parameters
        ----------
        order_id : str
        products : list of ReturnCreateProduct

        Returns
        -------
        The newly created `Return` object.
        """

        payload = {
            'order': order_id,
            'products': [
                {'sku': p.sku, 'reason': p.reason, 'note': p.reason}
                for p in products
            ]
        }

        r = self.__make_request(
            '/returns', method=requests.post, payload=payload
        )
        return Snapship.__parse_return(r)

    def __make_request(self, endpoint, method=requests.get, payload={}):
        url = '{protocol}://{host}/api/public{endpoint}'.format(
            protocol=self.protocol, host=self.host, endpoint=endpoint
        )
        headers = {'Authorization': 'Secret ' + self.secret_key}

        result = None

        page = 1
        total_pages = 1
        while page <= total_pages:
            params = {'page': page, 'limit': MAX_ITEMS_PER_PAGE}
            resp = method(url, headers=headers, params=params, json=payload)

            if not resp.ok:
                raise SnapshipResponseException(resp)

            resp_json = resp.json()

            if result is None:
                result = resp_json['result']
            else:
                result += resp_json['result']

            page += 1
            total_pages = resp_json.get('total_pages', 1)

        return result

    @staticmethod
    def __parse_order(o):
        return Order(
            o['id'], o['reference'],
            o['recipient_name'], o['recipient_email'],
            o['recipient_telephone'],
            Destination(**o['destination']),
            o['speed'], OrderStatus[o['status']], o['tracking_url'],
            Snapship.__parse_opt_utctimestamp(o['shipped_at']),
            Snapship.__parse_opt_utctimestamp(o['delivered_at']),
            [ProductRef(p['sku'], p['serial']) for p in o['products']],
            [ReturnRef(id) for id in o['returns']],
        )

    @staticmethod
    def __parse_product(p):
        return Product(
            p['sku'], p['name'],
            Inventory(**p['inventory']),
        )

    @staticmethod
    def __parse_return(r):
        return Return(
            r['id'], r['status'], r['label'],
            Snapship.__parse_opt_utctimestamp(r['processed_at']),
            Snapship.__parse_opt_utctimestamp(r['shipped_at']),
            Snapship.__parse_opt_utctimestamp(r['delivered_at']),
            [ProductRef(p['sku'], p['serial']) for p in r['products']],
        )

    @staticmethod
    def __parse_opt_utctimestamp(utctimestamp):
        if utctimestamp is not None:
            return datetime.datetime.utcfromtimestamp(utctimestamp)
        else:
            return None
