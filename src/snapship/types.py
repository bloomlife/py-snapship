# Bloom Technologies Inc. Copyright 2018
#
# Authors: Raphael Javaux <raphael@bloom-life.com>

import datetime

from collections import namedtuple
from enum import Enum

Destination = namedtuple('Destination', [
    'first_line', 'second_line', 'city', 'postal_code', 'state_province',
    'country',
])

Inventory = namedtuple('Inventory', [
    'available', 'inbound', 'on_hand', 'processing', 'reserved'
])

Order = namedtuple('Order', [
    'id', 'reference',
    'recipient_name', 'recipient_email', 'recipient_telephone', 'destination',
    'speed', 'status', 'tracking_url', 'shipped_at', 'delivered_at',
    'products', 'returns',
])

OrderStatus = Enum('OrderStatus', [
    'held', 'pending', 'picking', 'processing', 'shipped', 'delivered',
    'backordered', 'return pending', 'return en-route', 'return delivered',
    'return processed', 'returned', 'cancelled',
])

OrderCreateProduct = namedtuple('OrderCreateProduct', ['sku', 'quantity'])

Product = namedtuple('Product', ['sku', 'name', 'inventory'])

ProductRef = namedtuple('ProductRef', ['sku', 'serial'])

Recipient = namedtuple('Recipient', ['name', 'email', 'telephone',])

Return = namedtuple('Return', [
    'id', 'status', 'label',
    'processed_at', 'shipped_at', 'delivered_at',
    'products',
])

ReturnCreateProduct = namedtuple('OrderCreateProduct', [
    'sku', 'reason', 'note'
])

ReturnProduct = namedtuple('ReturnProduct', [
    'sku', 'serial', 'received', 'reason_note', 'reason', 'reception_note',
])

ReturnRef = namedtuple('ReturnRef', ['id'])
