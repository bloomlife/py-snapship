# Bloom Technologies Inc. Copyright 2018
#
# Authors: Raphael Javaux <raphael@bloom-life.com>

class SnapshipException(Exception):
    pass

class SnapshipResponseException(SnapshipException):
    def __init__(self, response):
        super().__init__(
            'Snapship API returned a non-OK status code: `{}`.'.format(
                response.status_code
            )
        )

        self.response = response
        self.error = response.json()['error']
